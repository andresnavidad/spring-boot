package es.nfq.activemq.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.jms.Destination;
import javax.jms.JMSException;
import java.util.Map;


@Component
public class Producer {

    private final static Logger LOG = LoggerFactory.getLogger(Producer.class);

    @Autowired
    private JmsTemplate jmsTemplate;


    public void sendTo(String destination, Object obj) {
        LOG.debug("Send msg: " + obj + " to: " + destination);
        sendToWithHeaders(destination, obj, null);
    }

    public void sendTo(Destination destination, Object obj) {
        LOG.debug("Send msg: " + obj + " to: " + destination.toString());
        sendToWithHeaders(destination, obj, null);
    }

    public void sendToWithHeaders(String destination, Object obj, Map<String, Object> headers) {

        if(!CollectionUtils.isEmpty(headers)) {
            LOG.debug("Send msg: " + obj + " to: " + destination);
            this.jmsTemplate.convertAndSend(destination, obj, includeHeaders(headers));
        } else {
            LOG.debug("Message has been sent without headers");
            this.jmsTemplate.convertAndSend(destination, obj);
        }

    }

    public void sendToWithHeaders(Destination destination, Object obj, Map<String, Object> headers) {

        if(!CollectionUtils.isEmpty(headers)) {
            LOG.debug("Send msg: " + obj + " to: " + destination.toString());
            this.jmsTemplate.convertAndSend(destination, obj, includeHeaders(headers));
        } else {
            LOG.warn("Message has been sent without headers");
            this.jmsTemplate.convertAndSend(destination, obj);
        }
    }


    private MessagePostProcessor includeHeaders(Map<String, Object> headers) {

        return message -> {
            headers.forEach((key, value) -> {
                try {
                    message.setObjectProperty(key, value);
                } catch (JMSException e) {
                    LOG.error("Impossible set headers", e);
                }
            });
            return message;
        };
    }


}
