package es.nfq.activemq.example.receiver;

import es.nfq.activemq.example.model.Email;
import es.nfq.activemq.example.model.User;
import org.slf4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.jms.JMSException;


@Component
public class Consumer {

    private final static Logger LOG = org.slf4j.LoggerFactory.getLogger(Consumer.class);

    //DEFAULT

    //Reading queue name from application.yml
    @JmsListener(destination = "${nfq.jms.queue.default}")
    public void receiveFromDefaultQueue(String text) throws JMSException {
        loggingObjectWithToString(text, "Default queue");
    }

    //Reading partial virtual topic name from application.yml
    @JmsListener(destination = "${nfq.jms.topic.default}")
    public void receiveFromDefaultTopic(String text) {
        loggingObjectWithToString(text, "Default topic");
    }

    //Reading partial virtual topic name from application.yml
    @JmsListener(destination = "Consumer.default_consumer_1.${nfq.jms.virtual-topic.default}")
    public void receiveFromDefaultVirtualTopic(String text) {
        loggingObjectWithToString(text, "Default virtual-topic");
    }

    //EMAIL

    //Reading queue name from application.yml
    @JmsListener(destination = "${nfq.jms.queue.email}")
    public void receiveFromEmailQueue(Message<Email> emailMessage) throws JMSException {
        loggingMessage(emailMessage);
        // do stuff
    }

    //Reading partial virtual topic name from application.yml
    @JmsListener(destination = "${nfq.jms.topic.email}")
    public void receiveFromEmailTopic(Message<Email> emailMessage) {
        loggingMessage(emailMessage);
        // do stuff
    }

    //Reading partial virtual topic name from application.yml
    @JmsListener(destination = "Consumer.consumer_topic_1.${nfq.jms.virtual-topic.email}")
    public void receiveFromEmailVirtualTopic(Message<Email> emailMessage) {
        loggingMessage(emailMessage);
    }

    //USER

    //Reading queue name from application.properties
    @JmsListener(destination = "${nfq.jms.queue.user}")
    public void receiveFromUserQueue(Message<User> userMessage) throws JMSException {
        loggingMessage(userMessage);
    }

    //Reading partial virtual topic name from application.properties
    @JmsListener(destination = "${nfq.jms.topic.user}")
    public void receiveFromUserTopic(Message<User> userMessage) {
        loggingMessage(userMessage);
    }

    //Reading partial virtual topic name from application.properties
    @JmsListener(destination = "Consumer.consumer_topic_1.${nfq.jms.virtual-topic.user}")
    public void receiveFromUserVirtualTopic(Message<User> userMessage) {
        loggingMessage(userMessage);
    }


    private void loggingObjectWithToString(Object obj, String channel) {
        Assert.notNull(obj, "Object parameter can not be null");
        LOG.debug("Channel: {} , Message: {}", channel, obj);
    }

    private <T> void loggingMessage(Message<T> message) {
        Assert.notNull(message, "message parameter can not be null");
        final T payload = message.getPayload();
        String channel = (String) message.getHeaders().get(MessageHeaders.REPLY_CHANNEL);
        loggingObjectWithToString(payload, channel);
    }

}
