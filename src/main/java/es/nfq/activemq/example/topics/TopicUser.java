package es.nfq.activemq.example.topics;

import org.apache.activemq.command.ActiveMQTopic;

/**
 * Created by navidad on 28/9/17.
 */
public class TopicUser extends ActiveMQTopic {

    public TopicUser(String name) {
        super(name);
    }
}
