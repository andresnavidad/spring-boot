package es.nfq.activemq.example.queues;

import org.apache.activemq.command.ActiveMQQueue;

/**
 * Created by navidad on 28/9/17.
 */
public class QueueUser extends ActiveMQQueue {

    public QueueUser(String name) {
        super(name);
    }
}
