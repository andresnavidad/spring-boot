package es.nfq.activemq.example.queues;

import org.apache.activemq.command.ActiveMQQueue;

/**
 * Created by navidad on 28/9/17.
 */
public class QueueEmail extends ActiveMQQueue {

    public QueueEmail(String name) {
        super(name);
    }
}
