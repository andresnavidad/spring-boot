package es.nfq.activemq.example;


import es.nfq.activemq.example.queues.QueueEmail;
import es.nfq.activemq.example.queues.QueueUser;
import es.nfq.activemq.example.topics.TopicEmail;
import es.nfq.activemq.example.topics.TopicUser;
import es.nfq.activemq.example.virtualtopics.VirtualTopicEmail;
import es.nfq.activemq.example.virtualtopics.VirtualTopicUser;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.jms.Queue;
import javax.jms.Topic;


@Component
public class ChannelsConfig {

    @Autowired
    private Environment env;


    @Bean(name = "defaultQueue")
    public Queue defaultQueue() {
        return new ActiveMQQueue(env.getProperty("nfq.jms.queue.default"));
    }

    @Bean(name = "defaultTopic")
    public Topic defaultTopic() {
        return new ActiveMQTopic(env.getProperty("nfq.jms.topic.default"));
    }

    @Bean(name = "defaultVirtualTopic")
    public Topic defaultVirtualTopic() {
        return new ActiveMQTopic(env.getProperty("nfq.jms.virtual-topic.default"));
    }

    @Bean
    public QueueUser userQueue() {
        return new QueueUser(env.getProperty("nfq.jms.queue.user"));
    }

    @Bean
    public TopicUser userTopic() {
        return new TopicUser(env.getProperty("nfq.jms.topic.user"));
    }

    @Bean
    public VirtualTopicUser userVirtualTopic() {
        return new VirtualTopicUser(env.getProperty("nfq.jms.virtual-topic.user"));
    }

    @Bean
    public QueueEmail emailQueue() {
        return new QueueEmail(env.getProperty("nfq.jms.queue.email"));
    }

    @Bean
    public TopicEmail emailTopic() {
        return new TopicEmail(env.getProperty("nfq.jms.topic.email"));
    }

    @Bean
    public VirtualTopicEmail emailVirtualTopic() {
        return new VirtualTopicEmail(env.getProperty("nfq.jms.virtual-topic.email"));
    }

}
