package es.nfq.activemq.example.virtualtopics;

import org.apache.activemq.command.ActiveMQTopic;

/**
 * Created by navidad on 28/9/17.
 */
public class VirtualTopicEmail extends ActiveMQTopic {

    public VirtualTopicEmail(String name) {
        super(name);
    }
}
