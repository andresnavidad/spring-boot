package es.nfq.activemq.example.virtualtopics;

import org.apache.activemq.command.ActiveMQTopic;

/**
 * Created by navidad on 28/9/17.
 */
public class VirtualTopicUser extends ActiveMQTopic {

    public VirtualTopicUser(String name) {
        super(name);
    }
}
