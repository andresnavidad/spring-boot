package es.nfq.activemq.example.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;


public class User {

    private String ID;
    private String name;
    private String surname;
    private Date birthDate;
    private Integer brotherCount;


    public User() {
    }

    public User(String ID, String name, String surname, Date birthDate, Integer brotherCount) {
        this.ID = ID;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.brotherCount = brotherCount;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getBrotherCount() {
        return brotherCount;
    }

    public void setBrotherCount(Integer brotherCount) {
        this.brotherCount = brotherCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return new EqualsBuilder()
                .append(ID, user.ID)
                .append(name, user.name)
                .append(surname, user.surname)
                .append(birthDate, user.birthDate)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(ID)
                .append(name)
                .append(surname)
                .append(birthDate)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("ID", ID)
                .append("name", name)
                .append("surname", surname)
                .append("birthDate", birthDate)
                .append("brotherCount", brotherCount)
                .toString();
    }
}
