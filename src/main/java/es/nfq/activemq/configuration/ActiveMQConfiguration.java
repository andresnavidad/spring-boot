package es.nfq.activemq.configuration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.jms.support.destination.DestinationResolver;
import org.springframework.stereotype.Component;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;


@Component
public class ActiveMQConfiguration {

    @Autowired
    private Environment env;

    /**
     * Strategy interface for resolving JMS destinations.
     *
     * If name ends with queue -> QUEUE
     * If name ends with topic -> TOPIC
     * If name contains VirtualTopic. -> Virtual topic is created.
     */
    @Component
    class NfqCustomDestinationResolver implements DestinationResolver{

        public Destination resolveDestinationName(Session session, String destinationName, boolean pubSubDomain) throws JMSException {
            if (destinationName.endsWith("queue") || destinationName.contains("VirtualTopic.")) {
                return session.createQueue(destinationName);
            } else if (destinationName.endsWith("topic")) {
                return session.createTopic(destinationName);
            }
            throw new RuntimeException("Naming convention not respected for destination " + destinationName);
        }
    }

    @Bean // Serialize message content to json using TextMessage
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

}
