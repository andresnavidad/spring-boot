package es.nfq.activemq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.core.env.SystemEnvironmentPropertySource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.util.ResourceUtils;

@SpringBootApplication
@EnableAutoConfiguration
@EnableJms
public class ActiveMQApplication {

    private final static Logger LOG = LoggerFactory.getLogger(ActiveMQApplication.class);

    @Autowired
    private Environment env;

    @Component
    @Profile("!test")
    class DefaultJobCommandLineRunner implements CommandLineRunner {
        private final Logger LOGJobDefault = LoggerFactory.getLogger(DefaultJobCommandLineRunner.class);

        @Override
        public void run(String... args) throws Exception {
            System.out.println(ClassUtils.getDefaultClassLoader().getParent().toString());
            System.out.println("env:" + env.getProperty("nfq.jar.exec"));
            final String property = env.getProperty("nfq.jar.exec");
            LOGJobDefault.debug(property);
            // do stuff
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(ActiveMQApplication.class, args);
    }

}


