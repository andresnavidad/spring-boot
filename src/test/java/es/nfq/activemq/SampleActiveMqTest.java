package es.nfq.activemq;

import es.nfq.activemq.example.queues.QueueEmail;
import es.nfq.activemq.example.queues.QueueUser;
import es.nfq.activemq.example.topics.TopicEmail;
import es.nfq.activemq.example.topics.TopicUser;
import es.nfq.activemq.example.virtualtopics.VirtualTopicEmail;
import es.nfq.activemq.example.virtualtopics.VirtualTopicUser;
import es.nfq.activemq.example.model.Email;
import es.nfq.activemq.example.model.User;
import es.nfq.activemq.producer.Producer;
import org.apache.commons.text.RandomStringGenerator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Topic;
import java.util.Random;
import java.util.stream.IntStream;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SampleActiveMqTest {

    private static final Logger LOG = LoggerFactory.getLogger(SampleActiveMqTest.class);

    private static final Random rand = new Random();

    private static final RandomStringGenerator generator = new RandomStringGenerator
            .Builder().withinRange('a', 'z').build();

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Environment env;

    @Autowired
    private Producer producer;

    @Resource
    private Queue defaultQueue;

    @Resource
    private Topic defaultTopic;

    @Resource
    private Topic defaultVirtualTopic;

    @Autowired
    private QueueUser userQueue;

    @Autowired
    private TopicUser userTopic;

    @Autowired
    private VirtualTopicUser userVirtualTopic;

    @Autowired
    private QueueEmail emailQueue;

    @Autowired
    private TopicEmail emailTopic;

    @Autowired
    private VirtualTopicEmail emailVirtualTopic;

    private final Integer NUM_MESSAGES = 10;

    @Test
    public void testSendSimpleTextOverMockQueue() throws InterruptedException, JMSException {

        String mockQueue = "mock.queue";
        String randString = generator.generate(4);
        producer.sendTo(mockQueue, randString);
        //Wait until received user
        final String stringReceived = (String) jmsTemplate.receiveAndConvert(mockQueue);
        Assert.assertEquals(randString, stringReceived);
    }

    @Test
    public void testSendSimpleTextOverMockQueue2() throws InterruptedException, JMSException {

        String mockQueue = "mock2.queue";
        User user = new User("1", "Andres", "Navidad", null, null);
        User user2 = new User("2", "Andres", "Navidad", null, null);
        producer.sendTo(mockQueue, user);
        producer.sendTo(mockQueue, user2);
        //Wait until received user
        final User userReceived = (User) jmsTemplate.receiveAndConvert(mockQueue);
        Assert.assertEquals(user, userReceived);
        final User userReceived2 = (User) jmsTemplate.receiveAndConvert(mockQueue);
        Assert.assertEquals(user2, userReceived2);
    }

    @Test
    public void testSendSimpleTextOverDefaultQueue() throws InterruptedException, JMSException {

        IntStream
                .range(0, NUM_MESSAGES)
                .forEach(x -> {
                    producer.sendTo(defaultQueue, generator.generate(4));
                    producer.sendTo(defaultTopic, generator.generate(4));
                    producer.sendTo(defaultVirtualTopic, generator.generate(4));
                });
    }

    @Test
    public void testSendSimpleTextOverUserQueue() throws InterruptedException, JMSException {

        IntStream
                .range(0, NUM_MESSAGES)
                .forEach(x -> {
                    producer.sendTo(userQueue, new User(String.valueOf(x), generator.generate(3), generator.generate(7), null, x));
                    producer.sendTo(userTopic, new User(String.valueOf(x), generator.generate(3), generator.generate(7), null, x));
                    producer.sendTo(userVirtualTopic, new User(String.valueOf(x), generator.generate(3), generator.generate(7), null, x));
                });
    }


    @Test
    public void testSendSimpleTextOverEmailQueue() throws InterruptedException, JMSException {
        double start = System.currentTimeMillis();
        IntStream
                .range(0, NUM_MESSAGES)
                .forEach(x -> {
                    producer.sendTo(emailQueue, new Email(generator.generate(4) + "@example.com", generator.generate(12)));
                    producer.sendTo(emailTopic, new Email(generator.generate(4) + "@example.com", generator.generate(12)));
                    producer.sendTo(emailVirtualTopic, new Email(generator.generate(4) + "@example.com", generator.generate(12)));
                });
        double stop = System.currentTimeMillis();
        double ratio = NUM_MESSAGES / ((stop - start) / 1000);
        LOG.debug("Send ratio msg/sec: " + ratio);
    }

}