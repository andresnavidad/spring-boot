package es.nfq.activemq;

import es.nfq.activemq.example.properties.DatasourceProperties;
import es.nfq.activemq.example.properties.ServerProperties;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by navidad on 27/9/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class LoadPropertiesTest {

    private static final Logger LOG = LoggerFactory.getLogger(LoadPropertiesTest.class);

    @Autowired
    private Environment env;

    @Autowired
    DatasourceProperties datasourceProperties;

    @Autowired
    ServerProperties serverProperties;

    @Test
    public void testLoadProperties() {

        LOG.debug("Testing load properties");


        //application.yml
        final String email = env.getProperty("server.email", "fake@nfq.es");
        Assert.assertEquals("andres.navidad@nfq.es", email);

        //ConfigurationProperties annotation with prefix from application.properties
        Assert.assertEquals("queue://19.02.02:/TEST", datasourceProperties.getUrl());

        //ConfigurationProperties annotation with prefix from application.yml
        Assert.assertEquals(3, serverProperties.getCluster().size());
    }
}
